import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Employee } from '../registration';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css'],
})
export class ReactiveComponent implements OnInit {
  emp: Employee;
  registForm: FormGroup;
  submit: boolean = false;
  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.registForm = this.formBuilder.group({
      title:['',Validators.required],
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      role: ['', Validators.required],
      location: ['', Validators.required],
    });
  }

  get f() { return this.registForm.controls; }

  OnSubmit() {
    this.submit = true;
    // display form values on success
    if (this.registForm.valid) {
      alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registForm.value));
      //assing form control values to model properties
      let user: Employee = new Employee();
      user.title =this.registForm.value['title']
      user.name = this.registForm.value['name'];
      user.email = this.registForm.value['email'];
      user.role = this.registForm.value['role'];
      user.location = this.registForm.value['location'];
      //once all model properties are assined we can service method to pass the model data to db table
      this.submit = false;
      this.registForm.reset();
    }
  }

  onReset() {
    this.submit = false;
    this.registForm.reset();
  }
}
